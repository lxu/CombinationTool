#!/usr/bin/env python
from __future__ import absolute_import

import copy
import logging
import os
import subprocess
import sys

import ROOT


__author__ = "Stefan Gadatsch"
__credits__ = ["Stefan Gadatsch"]
__version__ = "0.0.1"
__maintainer__ = "Stefan Gadatsch"
__email__ = "stefan.gadatsch@cern.ch"


FORMAT = '%(asctime)s - %(processName)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.DEBUG, format=FORMAT)
existing_logger = logging.getLogger('x')


class Snapshot(object):
    background = 0
    nominal = 1
    ucmles = 2


def unfold_constraints(initial, final, obs, nuis):
    itr = initial.createIterator()
    thisPdf = itr.Next()
    while thisPdf:
        nuis_tmp = nuis
        constraint_set = thisPdf.getAllConstraints(obs, nuis_tmp, False)
        if (thisPdf.IsA() is not ROOT.RooGaussian.Class()) and (thisPdf.IsA() is not ROOT.RooLognormal.Class()) and (thisPdf.IsA() is not ROOT.RooGamma.Class()) and (thisPdf.IsA() is not ROOT.RooPoisson.Class()) and (thisPdf.IsA() is not ROOT.RooBifurGauss.Class()):
            unfold_constraints(constraint_set, final, obs, nuis)
        else:
            final.add(thisPdf)
        thisPdf = itr.Next()


def make_asimov_data(ws, mc, data, Conditional, profileAt, generateAt):
    make_snapshot(ws, mc, data, profileAt, Conditional)
    if generateAt is Snapshot.ucmles and generateAt is not Snapshot.profileAt:
        make_snapshot(ws, mc, data, generateAt, Conditional)

    mu = mc.GetParametersOfInterest().first()
    muVal = mu.getVal()
    if generateAt is Snapshot.background:
        muVal = 0
    elif generateAt is Snapshot.nominal:
        muVal = 1
    elif generateAt is Snapshot.ucmles:
        ws.loadSnapshot("ucmles")
        muVal = mu.getVal()
        ws.loadSnapshot("nominalNuis")
        ws.loadSnapshot("nominalGlobs")
    else:
        print("Unknown value for generation requested.")
        return

    muStr = ""
    if profileAt is Snapshot.background:
        muStr = "_0"
    elif profileAt is Snapshot.nominal:
        muStr = "_1"
    elif profileAt is Snapshot.ucmles:
        muStr = "_muhat"
    else:
        print("Unknown value for profiling requested.")
        return

    if not Conditional:
        ws.loadSnapshot("nominalGlobs")
        ws.loadSnapshot("nominalNuis")

    ws.loadSnapshot("nominalGlobs")
    ws.loadSnapshot("conditionalNuis" + muStr)

    mu.setVal(muVal)

    # TODO(Binning) Add option to modify binning of observables

    pois = mc.GetParametersOfInterest()
    genPoiValues = pois.snapshot()
    allParams = mc.GetPdf().getParameters(data)
    ROOT.RooStats.RemoveConstantParameters(allParams)
    allParams = genPoiValues
    globs = mc.GetGlobalObservables()

    thisAsimovData = ROOT.RooStats.AsymptoticCalculator.MakeAsimovData(mc, allParams, globs)
    # thisAsimovData.SetName("asimovData" + (muStr if profileAt is generateAt else ""))
    # thisAsimovData.SetTitle("asimovData" + (muStr if profileAt is generateAt else ""))
    thisAsimovData.SetName("asimovData" + muStr)
    thisAsimovData.SetTitle("asimovData" + muStr)
    getattr(ws, 'import')(thisAsimovData)

    ws.loadSnapshot("nominalNuis")
    ws.loadSnapshot("nominalGlobs")


def make_snapshot(ws, mc, data, thisSnapshot, Conditional):
    print("Making snapshot")

    mu = mc.GetParametersOfInterest().first()
    muVal = mu.getVal()
    if thisSnapshot is Snapshot.background:
        muVal = 0
    elif thisSnapshot is Snapshot.nominal:
        muVal = 1
    elif thisSnapshot is Snapshot.ucmles:
        # Will be set later
        muVal = 1
    else:
        print("Unknown snapshot requested.")
        return

    mu.setVal(muVal)

    muStr = ""
    if thisSnapshot is Snapshot.background:
        muStr = "_0"
    elif thisSnapshot is Snapshot.nominal:
        muStr = "_1"
    elif thisSnapshot is Snapshot.ucmles:
        muStr = "_muhat"
    else:
        print("Unknown snapshot requested.")
        return

    mc_obs = mc.GetObservables()
    mc_globs = mc.GetGlobalObservables()
    mc_nuis = mc.GetNuisanceParameters()

    # Pair the nuisance parameters and global observables
    mc_nuis_tmp = copy.deepcopy(mc_nuis)
    nui_list = ROOT.RooArgList("ordered_nuis")
    glob_list = ROOT.RooArgList("ordered_globs")
    pdf = mc.GetPdf()
    constraint_set_tmp = ROOT.RooArgSet(pdf.getAllConstraints(mc_obs, mc_nuis_tmp, False))
    constraint_set = ROOT.RooArgSet()

    unfold_constraints(constraint_set_tmp, constraint_set, mc_obs, mc_nuis_tmp)

    cIter = constraint_set.createIterator()
    arg = cIter.Next()
    while arg:
        thisNui = None
        nIter = mc_nuis.createIterator()
        nui_arg = nIter.Next()
        while nui_arg:
            if arg.dependsOn(nui_arg):
                thisNui = nui_arg
                break
            nui_arg = nIter.Next()

        components = arg.getComponents()
        components.remove(arg)
        if components.getSize():
            itr1 = components.createIterator()
            arg1 = itr1.Next()
            while arg1:
                itr2 = components.createIterator()
                arg2 = itr2.Next()
                while arg2:
                    if arg1 == arg2:
                        arg2 = itr2.Next()
                        continue
                    if arg2.dependsOn(arg1):
                        components.remove(arg1)
                    arg2 = itr2.Next()
                arg1 = itr1.Next()

        if components.getSize() > 1:
            print("Failed to isolate proper nuisance parameter")
            return
        elif components.getSize() == 1:
            thisNui = components.first()

        thisGlob = None
        gIter = mc_globs.createIterator()
        glob_arg = gIter.Next()
        while glob_arg:
            if arg.dependsOn(glob_arg):
                thisGlob = glob_arg
                break
            glob_arg = gIter.Next()

        if (thisNui is None):
            print("Failed to find nuisance parameter for constraint " + arg.GetName())
            arg = cIter.Next()
            continue

        if (thisGlob is None):
            print("Failed to find global observable for constraint " + arg.GetName())
            arg = cIter.Next()
            continue

        print("Pairing nuisance parameter " + thisNui.GetName() + " with global observable " + thisGlob.GetName() + " from constraint " + arg.GetName())
        nui_list.add(thisNui)
        glob_list.add(thisGlob)

        arg = cIter.Next()

    if nui_list.getSize() != glob_list.getSize():
        print("Size does not match")
        return

    print("Saving nominal snapshots")
    ws.saveSnapshot("nominalGlobs", mc.GetGlobalObservables())
    ws.saveSnapshot("nominalNuis", mc.GetNuisanceParameters())

    mu.setVal(muVal)
    if (thisSnapshot == Snapshot.background) or (thisSnapshot == Snapshot.nominal):
        mu.setConstant(True)
    else:
        mu.setConstant(False)

    if Conditional:
        print("Performing conditional fit")

        # TODO(PDF arguments) Add handle to set arguments on pdfs, e.g. turn off level 2 constant term optimization
        # TODO(ExtendendMinimizer) Switch to ExtendendMinimizer
        nuis = mc.GetNuisanceParameters()
        globs = mc.GetGlobalObservables()
        result = pdf.fitTo(data, ROOT.RooFit.Minimizer("Minuit2", "minimize"), ROOT.RooFit.Strategy(1), ROOT.RooFit.Constrain(nuis), ROOT.RooFit.GlobalObservables(globs), ROOT.RooFit.Hesse(False), ROOT.RooFit.Offset(True), ROOT.RooFit.Optimize(2))
        if result.status is not 0:
            print("Conditional fit failed")

    mu.setConstant(False)

    for i in range(0, nui_list.getSize()):
        nui = nui_list.at(i)
        glob = glob_list.at(i)

        print("Setting global observable " + glob.GetName() + " (previous value: " + str(glob.getVal()) + ") to conditional value: " + str(nui.getVal()))

        glob.setVal(nui.getVal())

    print("Saving conditional snapshots")
    ws.saveSnapshot("conditionalGlobs" + muStr, mc.GetGlobalObservables())
    ws.saveSnapshot("conditionalNuis" + muStr, mc.GetNuisanceParameters())

    if thisSnapshot == Snapshot.ucmles:
        nuisAndPOI = mc.GetNuisanceParameters()
        nuisAndPOI.add(mu)
        ws.saveSnapshot("ucmles", nuisAndPOI)

    ws.loadSnapshot("nominalNuis")
    ws.loadSnapshot("nominalGlobs")


if __name__ == '__main__':
    pass
